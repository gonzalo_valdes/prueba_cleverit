Feature: Acceder a Gmail
  Scenario Outline: Como usuario quiero ingresar a la aplicacion de correos Gmail.
    Given : Que ingrese al navegador de chrome
    And : Ingreso a la "<url>"
    When : Ingresar "<correo>"
    And : Presionar Siguiente
    And : Introducir "<contraseña>"
    And : Presionar Siguiente
    Then : Se debe realizar ingreso al correo en estado "<status>"

    Examples:
      | url             | correo             | contraseña  | status   |
      | www.gmail.com   | correo@gmail.com   | contraseña  | correcto |
      | www.gmail.com   | correo@gmail.com   | passErronea | fallido  |
