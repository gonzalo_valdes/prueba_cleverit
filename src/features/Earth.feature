Feature: Acceder a Google Earth
  Scenario Outline: Como usuario quiero ingresar a chrome para ver la pagina de Google Earth.
    Given : Que ingrese al navegador de chrome
    And : Ingreso a la "<url>"
    When : Introduzco en el input "<buscar>" la palabra a buscar
    And : Presionar enter
    And : presionar "<hyperlink>" Google Earth
    Then : Se debe visualizar en el navegador la pagina principal de Google Earth

    Examples:
      | url              | buscar         | hyperlink    |
      | www.google.com   |  Google Earth  | Google Earth |
