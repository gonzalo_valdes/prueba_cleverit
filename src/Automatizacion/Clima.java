package Automatizacion;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

public class Clima {
    //Instanciar objeto webdriver
    static WebDriver driver;
    public static void main(String[] args) {
        String chromePath = System.getProperty("user.dir") + "\\drivers\\chromedriver.exe";
        String url = "http://www.google.com/";
        System.setProperty("webdriver.chrome.driver", chromePath);
        driver = new ChromeDriver();
        driver.get(url);
        driver.manage().window().maximize();
        try{
            String buscar = "Clima";
            WebElement txtBuscar = driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input"));
            txtBuscar.sendKeys(buscar);
            Thread.sleep(2000);
            txtBuscar.sendKeys(Keys.ENTER);
            WebElement palPagina = driver.findElement(By.xpath("//*[@id=\"tsf\"]/div[1]/div[1]/div[2]/div/div[2]/input"));
            String value = palPagina.getAttribute("value");
            System.out.println(value);
            Thread.sleep(2000);
            if(value.equals(buscar)){
                System.out.println("Se encontro la pagina del clima");
            }else{
                System.out.println("No se logro reconocer la pagina");
            }
        }catch (NoSuchElementException ne){
            System.err.println("El elemento no se encontro "+ne.getMessage());
        }catch (WebDriverException wde){
            System.err.println("Fallo en WebDriver "+wde.getMessage());
        }catch (Exception e){
            System.err.println("Fallo la prueba por: "+e.getMessage());
        }
        finally {
            driver.close();
        }
    }
}
