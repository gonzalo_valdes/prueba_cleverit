Feature: Acceder a ver el clima
  Scenario Outline: Como usuario quiero ingresar a chrome para observar el clima a través del navegador.
    Given : Que ingrese al navegador de chrome
    And : Ingreso a la "<url>"
    When : Introduzco en el input "<buscar>" la palabra a buscar
    And : Presionar enter
    Then : Se debe visualizar en el navegador el clima actual y de los siguientes 7 dias

    Examples:
      | url              | buscar |
      | www.google.com   | clima  |
