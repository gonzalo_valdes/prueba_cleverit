Feature: Validar campo null
  Scenario Outline: Como usuario quiero validar como responde el servicio en caso de no enviarse el campo
    Given : Que se carge la herramienta con el servicio
    And : El campo "<agreement>" debe estar vacio
    When : Presiono ejecutar
    Then : La herramienta de testeo debe responder "<status>"

    Examples:
      | agreement | status                        |
      |           | Error falta campo obligatorio |