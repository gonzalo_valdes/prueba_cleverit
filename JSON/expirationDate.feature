Feature: Validar formato del campo
  Scenario Outline: Como usuario quiero validar como responde el servicio en caso de recibir un formato inadecuado
    Given : Que se carge la herramienta con el servicio
    And : El campo "<expirationDate>" debe con otro formato
    When : Presiono ejecutar
    Then : La herramienta de testeo debe responder "<status>"

    Examples:
      | expirationDate | status                 |
      | 31/01/2021     | Error formato invalido |