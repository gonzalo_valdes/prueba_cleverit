Feature: Validar tipo de dato String
  Scenario Outline: Como usuario quiero validar como responde el servicio en caso de recibir un tipo de dato no correspondiente
    Given : Que se carge la herramienta con el servicio
    And : El campo "<cardType>" debe tener un dato tipo String
    When : Presiono ejecutar
    Then : La herramienta de testeo debe responder "<status>"

    Examples:
      | cardType  | status                        |
      | prueba    | Error dato ingresado invalido |