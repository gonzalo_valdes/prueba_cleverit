Feature: Validar largo del campo
  Scenario Outline: Como usuario quiero validar como responde el servicio en caso de recibir un largo que supere el tipo de dato del campo
    Given : Que se carge la herramienta con el servicio
    And : El campo "<amount>" debe tener un dato que supere su largo
    When : Presiono ejecutar
    Then : La herramienta de testeo debe responder "<status>"

    Examples:
      | amount                              | status                              |
      | 99999999999999999999999999999999999 | Error al superar el largo del campo |