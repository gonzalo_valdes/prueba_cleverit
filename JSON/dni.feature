Feature: Validar tipo de dato Int
  Scenario Outline: Como usuario quiero validar como responde el servicio en caso de recibir un tipo de dato no correspondiente
    Given : Que se carge la herramienta con el servicio
    And : El campo "<dni>" debe tener un dato tipo Int
    When : Presiono ejecutar
    Then : La herramienta de testeo debe responder "<status>"

    Examples:
      | dni     | status                        |
      | 111111  | Error dato ingresado invalido |